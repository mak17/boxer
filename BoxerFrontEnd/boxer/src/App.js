import React from 'react';
import './App.css';
import './Style/Boxer.css'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import TopMenu from './Components/TopMenu';
import InfoTab from './Components/InfoTab'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ProfilPage from './Components/Profil/ProfilPage';
import TrainingPage from './Components/Training/TrainingPage';
import WorkPage from './Components/Work/WorkPage';
import RestaurantPage from './Components/Restaurant/RestaurantPage';
import RestPage from './Components/Rest/RestPage';
import LiguePage from './Components/Ligue/LiguePage';
import ChampionshipPage from './Components/Championship/ChampionshipPage';
import SettingsPage from './Components/Settings/SettingsPage';
import { createStore } from 'redux';
import boxerReducer from './Redux/index';
import { Provider } from 'react-redux';

const store = createStore(boxerReducer);

window.store = store;
function App() { 

  const changePage = (pageName) => {  
    let linkElement = document.getElementById(pageName);
    linkElement.click();
  };

  return (
    <Container>
      <Provider store={store}>
        <Router>
          <Link id="Profil" to={"/Profil"}></Link>
          <Link id="Training" to={"/Training"} ></Link>
          <Link id="Work" to={"/Work"} ></Link>
          <Link id="Restaurant" to={"/Restaurant"} ></Link>
          <Link id="Rest" to={"/Rest"} ></Link>
          <Link id="Ligue" to={"/Ligue"} ></Link>
          <Link id="Championship" to={"/Championship"} ></Link>
          <Link id="Settings" to={"/Settings"} ></Link>
          <Row>
            <Col sm={12}>
              <TopMenu handleOnTabChange={changePage} />
            </Col>
          </Row>
          <Row>
            <Col sm={3}>
              <InfoTab />
            </Col>
            <Col sm={8}>
                <Switch>
                  <Route path="/Profil">        <ProfilPage />        </Route>
                  <Route path="/Training">      <TrainingPage />      </Route>
                  <Route path="/Work">          <WorkPage />          </Route>
                  <Route path="/Restaurant">    <RestaurantPage />    </Route>
                  <Route path="/Rest">          <RestPage />          </Route>
                  <Route path="/Ligue">         <LiguePage />         </Route>
                  <Route path="/Championship">  <ChampionshipPage />  </Route>
                  <Route path="/Settings">      <SettingsPage />      </Route>
                </Switch>
            </Col>
          </Row>
        </Router>
      </Provider>
    </Container>
  );
}

export default App;
