import boxerReducer from './reducers';
export { default as boxerTypes } from './types';
export { default as boxerActions } from './actions';

export default boxerReducer