import types from './types';

export const addBoxerAction = newAction => ({
    type: 'ADD_BOXER_ACTIONS', newAction
});

export const removeBoxerAction = () => ({
    type: 'REMOVE_BOXER_ACTIONS'
});

export const setBoxer = boxer => ({
    type: 'SET_BOXER', boxer
});

export default {
    addBoxerAction,
    removeBoxerAction,
    setBoxer
}