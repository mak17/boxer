import types from './types';

const initialState = {
  boxerActions: [],
  boxer: {}
};

const boxerReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_BOXER_ACTIONS':
      return {...state, boxerActions: [...state.boxerActions, action.newAction]};
    case 'REMOVE_BOXER_ACTIONS':
      let actions = [...state.boxerActions];
      actions.splice(0, 1);
      return {...state, boxerActions: actions};
    case 'SET_BOXER':
      return {...state, boxer: action.boxer};
    default:
      return state
  }
}

export default boxerReducer
