export const getCurrentDate = () => {
    const today = new Date();
    return today.getFullYear() + '-' + getNumber(today.getMonth()+1) + '-' + getNumber(today.getDate());
};

export const getNumber = (days) => {
    return days > 9 ? days : "0" + days;
};

export const getDate = (date) => {
    let date_ = new Date(date);
    date_ = date_.getFullYear() + '-' + getNumber(date_.getMonth()+1) + '-' + getNumber(date_.getDate());
    
    return date_;
}