const apiUrl = "http://localhost:8080/api/";

export const getDataAPI = async (id, paramUrl, callback) => {
    const url = apiUrl + paramUrl + id;

    await fetch(url).then(response => {
       
        switch(response.status) {
            case 200: 
                response.json().then(boxer => {
                    callback(boxer, null);
                });
                break;
            case 401: 
                throw new Error('Unauthorized Error');
            case 500: 
                throw new Error('Internal Server Error');
            default:
                throw new Error('Something went wrong');
        }
    }).catch( error => callback(null, error) );
}

export const sendDataAPI = async (id, endPoint, params, callback) => {
    const url = apiUrl + endPoint + id + "/" + params;

    console.log(url)
    await fetch(url).then(response => {
       
        switch(response.status) {
            case 200: 
                response.json().then(boxer => {
                    callback(boxer, null);
                });
                break;
            case 401: 
                throw new Error('Unauthorized Error');
            case 500: 
                throw new Error('Internal Server Error');
            default:
                throw new Error('Something went wrong');
        }
    }).catch( error => callback(null, error) );
}
