import React, { Component } from 'react';
import labels from './../../Labels/Labels';
import { getDataAPI } from './../../Api/Api';
import { trainingUrl } from './../../Urls/Urls';
import {Growl} from 'primereact/growl';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Card } from 'primereact/card';
import {Button} from 'primereact/button';
import { connect } from 'react-redux';
import { addBoxerAction, setBoxer } from '../../Redux/actions';

class TrainingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trainings: [],
            isActionListFull: false
        };
    };

    componentDidMount() {
        this.setState({isActionListFull: this.props.boxerActions.length >= 3});
        
        const boxerLevel = 2;
        getDataAPI(boxerLevel, trainingUrl, (trainings, error) => { 

            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                this.setState({ trainings: trainings });
            }
        });
    };

    trainHandle(training, event) {
        let action = training;
        action.type = "TRAINING";

        if (this.state.isActionListFull) {
            this.growl.show({severity: 'info', summary: "Action queue", detail: "There can only be 3 options in the queue"});
        } else {
            this.props.addBoxerAction(action);
        }
    };

    componentDidUpdate(prevProps) {
        if(prevProps.boxerActions !== this.props.boxerActions)
           this.setState({isActionListFull: this.props.boxerActions.length >= 3});
    };

    render() {

        const { trainings } = this.state;

        return (
            <div  className="mt30"> 
                <Growl ref={(el) => this.growl = el} />
                <h1>{labels.training.title}</h1> 
                <Row>
                    {trainings.map(training => <Col sm={4} key={training.id} className="mt30">
                            <Card title={training.name} 
                                subTitle={training.price + " PLN"}
                                footer={<Button label={labels.training.train}
                                            style={{width: "100%"}}
                                            onClick={(event) => this.trainHandle(training, event)}>
                                    </Button>}>
                                <Row>
                                    <Col sm={6}>
                                        {labels.training.time}
                                    </Col>
                                    <Col sm={6}>
                                        {training.time + " min"} 
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6}>
                                        {labels.training.energy}
                                    </Col>
                                    <Col sm={6}>
                                        {"-"     + training.energy} 
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6}>
                                        {labels.training.attack}
                                    </Col>
                                    <Col sm={6}>
                                        {"+"     + training.attack} 
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6}>
                                        {labels.training.defend}
                                    </Col>
                                    <Col sm={6}>
                                        {"+"     + training.defend} 
                                    </Col>
                                </Row>
                            </Card>
                        </Col>)}
                </Row>
            </div>
        );
    };
};

const mapStateToProps = state => ({
    boxerActions: state.boxerActions,
    boxer: state.boxer
});

const mapDispatchToProps = dispatch => ({
    addBoxerAction: boxerAction => dispatch(addBoxerAction(boxerAction)),
    setBoxer: boxer => dispatch(setBoxer(boxer))
}); 

export default connect(mapStateToProps, mapDispatchToProps)(TrainingPage)