import React, { Component } from 'react';
import labels from './../../Labels/Labels';
import { getDataAPI, sendDataAPI } from '../../Api/Api';
import { foodsUrl, updateBoxerUrl } from './../../Urls/Urls';
import {Growl} from 'primereact/growl';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Card } from 'primereact/card';
import {Button} from 'primereact/button';

class RestaurantPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foods: []
        };
    };

    componentDidMount() {
        
        const boxerLevel = 2;
        getDataAPI(boxerLevel, foodsUrl, (foods, error) => { 

            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                console.log(foods)
                this.setState({ foods: foods });
            }
        });
    };

    eatFood = food => this.updateBoxer(food);

    updateBoxer = action => {
        const params = action.id + "/" + "RESTAURANT";
        const boxerId = 10;

        sendDataAPI(boxerId, updateBoxerUrl, params,  (boxer, error) => {
            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                let boxer_ = { 
                    name: boxer.name,
                    surname: boxer.surname,
                    money: boxer.money,
                    energy: boxer.energy,
                    defend: boxer.defend,
                    attack: boxer.attack,
                    level: boxer.level
                };
                this.props.setBoxer(boxer_);
            }
        });
    };

    render() {

        const { foods } = this.state;

        return (
            <div  className="mt30"> 
                <Growl ref={(el) => this.growl = el} />
                <h1>{labels.foods.title}</h1> 
                <Row>
                    {foods.map(food => <Col sm={4} key={food.id} className="mt30">
                            <Card title={food.name} 
                                subTitle={food.price + " PLN"}
                                footer={<Button label={labels.foods.eat}
                                            style={{width: "100%"}}
                                            onClick={(event) => this.eatFood(food, event)}>
                                    </Button>}>
                                <Row>
                                    <Col sm={6}>
                                        {labels.foods.energy}
                                    </Col>
                                    <Col sm={6}>
                                        {"+"     + food.energy} 
                                    </Col>
                                </Row>
                            </Card>
                        </Col>)}
                </Row>
            </div>
        );
    };
};

export default RestaurantPage