import React, { Component } from 'react';
import Trophies from './Trophies';
import Statistics from './Statistics';

class ProfilPage extends Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <div className="mt30">
               <Trophies />
               <div className="mt30" > 
                    <Statistics />
               </div>
            </div>
        );
    };
};

export default ProfilPage