import React, { Component } from 'react';
import { boxerUrl } from './../../Urls/Urls';
import { getDataAPI } from './../../Api/Api';
import {Growl} from 'primereact/growl';
import labels from './../../Labels/Labels';
import { Chart } from 'primereact/chart';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Card } from 'primereact/card';

class Statistics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statistics: [],
            chartData: {}
        };
    };

    componentDidMount() {
        const boxerId = 10;

        getDataAPI(boxerId, boxerUrl, (statistics, error) => { 

            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                const statistics_ = { 
                    won: statistics.won,
                    ko: statistics.ko,
                    wonRow: statistics.wonRow,
                    lost: statistics.lost,
                    lostRow: statistics.lostRow,
                    draws: statistics.draws,
                    fight: statistics.fight
                };

                const chartData_ = {
                    labels: ['Won','Lost','Draws'],
                    datasets: [
                    {
                        data: [statistics.won, statistics.lost, statistics.draws],
                        backgroundColor: [
                            "#008000",
                            "#36A2EB",
                            "#FFCE56"
                        ],
                        hoverBackgroundColor: [
                            "#008000",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }]
                };
               
                this.setState({
                    statistics: statistics_,
                    chartData: chartData_
                });
            }
        });

    };



    render() {

        const { chartData, statistics } = this.state;

        return (
            <div>
                <Growl ref={(el) => this.growl = el} />
                <Row>
                    <h2>{labels.profil.statistics.title}</h2>
                </Row>
                <Row>
                    <Col sm={6}>
                        <Chart type="doughnut" data={chartData} />
                    </Col>
                    <Col sm={6}>
                        <Card title={labels.profil.statistics.cardTitle}>
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.fight}
                                </Col>
                                <Col sm={6}>
                                    {statistics.fight}
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.won}
                                </Col>
                                <Col sm={6}>
                                    {statistics.won}
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.ko}
                                </Col>
                                <Col sm={6}>
                                    {statistics.ko}
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.wonRow}
                                </Col>
                                <Col sm={6}>
                                    {statistics.wonRow}
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.lost}
                                </Col>
                                <Col sm={6}>
                                    {statistics.lost}
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.lostRow}
                                </Col>
                                <Col sm={6}>
                                    {statistics.lostRow}
                                </Col>
                            </Row> 
                            <Row>
                                <Col sm={6}>
                                    {labels.profil.statistics.draws}
                                </Col>
                                <Col sm={6}>
                                    {statistics.draws}
                                </Col>
                            </Row> 
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    };
};

export default Statistics