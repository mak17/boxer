import React, { Component } from 'react';
import { Carousel } from 'primereact/carousel';
import { Card } from 'primereact/card';
import { trophiesUrl } from './../../Urls/Urls';
import { getDataAPI } from './../../Api/Api';
import {Growl} from 'primereact/growl';
import { getDate } from './../../Services/DateService';
import labels from './../../Labels/Labels';

class Trophies extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trophies: []
        };
    };

    componentDidMount() {

        const imageUrl = "./../../images/";
        let trophies_ = {};
        const boxerId = 10;

        getDataAPI(boxerId, trophiesUrl, (trophies, error) => { 
            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                trophies_ = this.convertDate(trophies);
                this.setState({trophies: trophies_});
            }
        });
    };

    convertDate = (trophies) => {
        
        trophies.forEach(trophy => {
            trophy.dateAward = getDate(trophy.dateAward);
        });

        return trophies;
    }

    trophyTemplate(trophy) {

        const header = ( <img src={require("./../../images/" + trophy.image + ".jpg")} /> );
        return <Card title={trophy.name} subTitle={trophy.dateAward} style={{width: '100%'}} header={header} className="ui-card-shadow">
                    <div>{trophy.description}</div>
                </Card>
    };

    render() {

        const { trophies } = this.state;
        
        return (
            <div>
                <Growl ref={(el) => this.growl = el} />
                <h2>{labels.profil.trophy.title}</h2>
                <Carousel value={trophies} itemTemplate={this.trophyTemplate}  numVisible={3} numScroll={1} autoplayInterval={2000} circular={true}></Carousel>
            </div>
        );
    };
};

export default Trophies