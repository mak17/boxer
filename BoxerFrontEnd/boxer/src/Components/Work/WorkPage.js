import React, { Component } from 'react';
import labels from './../../Labels/Labels';
import { getDataAPI } from './../../Api/Api';
import { workUrl } from './../../Urls/Urls';
import {Growl} from 'primereact/growl';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Card } from 'primereact/card';
import {Button} from 'primereact/button';
import { connect } from 'react-redux';
import { addBoxerAction, setBoxer } from '../../Redux/actions';

class WorkPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            works: [],
            isActionListFull: false
        };
    };

    componentDidMount() {
        this.setState({isActionListFull: this.props.boxerActions.length >= 3});
        
        const boxerLevel = 2;
        getDataAPI(boxerLevel, workUrl, (works, error) => { 

            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                this.setState({ works: works });
            }
        });
        
    };

    componentDidUpdate(prevProps) {
        if(prevProps.boxerActions !== this.props.boxerActions)
           this.setState({isActionListFull: this.props.boxerActions.length >= 3});
    };

    workHandle(work, event) {
        let action = work;
        action.type = "WORK";

        if (this.state.isActionListFull) {
            this.growl.show({severity: 'info', summary: "Action queue", detail: "There can only be 3 options in the queue"});
        } else {
            this.props.addBoxerAction(action);
        }
    };

    render() {

        const { works } = this.state;

        return (
            <div  className="mt30"> 
                <Growl ref={(el) => this.growl = el} />
                <h1>{labels.works.title}</h1> 
                <Row>
                    {works.map(work => <Col sm={4} key={work.id} className="mt30">
                            <Card title={work.name} 
                                subTitle={work.salary + " PLN"}
                                footer={<Button label={labels.works.work}
                                            style={{width: "100%"}}
                                            onClick={(event) => this.workHandle(work, event)}>
                                    </Button>}>
                                <Row>
                                    <Col sm={6}>
                                        {labels.works.time}
                                    </Col>
                                    <Col sm={6}>
                                        {work.time + " min"} 
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6}>
                                        {labels.works.energy}
                                    </Col>
                                    <Col sm={6}>
                                        {"-"     + work.energy} 
                                    </Col>
                                </Row>
                            </Card>
                        </Col>)}
                </Row>
            </div>
        );
    };
};

const mapStateToProps = state => ({
    boxerActions: state.boxerActions,
    boxer: state.boxer
});

const mapDispatchToProps = dispatch => ({
    addBoxerAction: boxerAction => dispatch(addBoxerAction(boxerAction)),
    setBoxer: boxer => dispatch(setBoxer(boxer))
}); 

export default  connect(mapStateToProps, mapDispatchToProps)(WorkPage)