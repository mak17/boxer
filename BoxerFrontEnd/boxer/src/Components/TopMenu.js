import React from 'react';
import labels from '../Labels/Labels';
import { TabMenu } from 'primereact/tabmenu';

class TopMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            tabMenu: [],
            activeItem: "/"
        };
    };

    componentDidMount() {

        const tabMenu = [
            {label: labels.tabMenu.profil, icon: 'pi pi-fw pi-user'},
            {label: labels.tabMenu.training, icon: 'pi pi-fw pi-chart-line'},
            {label: labels.tabMenu.work, icon: 'pi pi-fw pi-briefcase'},
            {label: labels.tabMenu.restaurant, icon: 'pi pi-fw pi-star'},
            {label: labels.tabMenu.rest, icon: 'pi pi-fw pi-heart'},
            {label: labels.tabMenu.ligue, icon: 'pi pi-fw pi-calendar'},
            {label: labels.tabMenu.championship, icon: 'pi pi-fw pi-users'},
            {label: labels.tabMenu.settings, icon: 'pi pi-fw pi-cog'},
            {label: labels.tabMenu.logout, icon: 'pi pi-fw pi-power-off'}
        ];

        this.setState({tabMenu: tabMenu });
    };

    changePage = event => {
        this.setState({activeItem: event.value});
        this.props.handleOnTabChange(event.value.label);
    };

    render() {

        const { tabMenu, activeItem } = this.state;

        return (
            <div>
               <TabMenu model={tabMenu} activeItem={activeItem} onTabChange={event => this.changePage(event)}/>
            </div>
        );
    };
};

export default TopMenu
