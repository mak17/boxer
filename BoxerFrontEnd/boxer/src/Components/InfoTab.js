import React from 'react';
import { Card } from 'primereact/card';
import labels from '../Labels/Labels';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import * as DateService from '../Services/DateService.js';
import { ProgressBar } from 'primereact/progressbar';
import {Growl} from 'primereact/growl';
import { getDataAPI, sendDataAPI } from '../Api/Api';
import { boxerUrl, updateBoxerUrl } from '../Urls/Urls';
import { connect } from 'react-redux';
import { removeBoxerAction, setBoxer } from '../Redux/actions';

class InfoTab extends React.Component {
    constructor() {
        super();
        this.state = {
            date: "",
            boxer: {},
            progressValue: 100,
            progressTitle: "",
            isProgress: false
        };
    };

    componentDidMount() {
        const boxerId = 10;

        getDataAPI(boxerId, boxerUrl, (boxer, error) => { 

            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                let boxer_ = { 
                    name: boxer.name,
                    surname: boxer.surname,
                    money: boxer.money,
                    energy: boxer.energy,
                    defend: boxer.defend,
                    attack: boxer.attack,
                    level: boxer.level
                }
                this.setState({
                    date: DateService.getCurrentDate(),
                    boxer: boxer_
                });
                this.props.setBoxer(boxer_);
            }
        });
        
    };


    componentDidUpdate(prevProps) {

        const { boxer, boxerActions } = this.props;

        if( prevProps.boxer !== boxer) {    
            let boxer_ = { 
                name: boxer.name,
                surname: boxer.surname,
                money: boxer.money,
                energy: boxer.energy,
                defend: boxer.defend,
                attack: boxer.attack,
                level: boxer.level
            }
            this.setState({ boxer: boxer_});
      }

        if( prevProps.boxerActions !== boxerActions && boxerActions.length > 0 ) {

                if(!this.state.isProgress ) {

                    if(this.isPossibleTraining()) {

                        this.setState({
                            progressValue: 0,
                            progressTitle: boxerActions[0].name,
                            isProgress: true
                        });
                        this.updateBoxer(boxerActions[0]);
                        this.runProgressBar(boxerActions[0].time,);
                        
                    } else {
                        const title = boxerActions[0].name + " is not possible !!!!"
                        this.growl.show({severity: 'error', summary: title, detail: "Boxer has not enough energy or money"});
                        this.props.removeBoxerAction();
                    }
                }   
          }
    };

    updateBoxer = action => {
        const params = action.id + "/" + action.type;
        const boxerId = 10;

        sendDataAPI(boxerId, updateBoxerUrl, params,  (boxer, error) => {
            if (error) {
                this.growl.show({severity: 'error', summary: error.name, detail: error.message});
            } else {
                let boxer_ = { 
                    name: boxer.name,
                    surname: boxer.surname,
                    money: boxer.money,
                    energy: boxer.energy,
                    defend: boxer.defend,
                    attack: boxer.attack,
                    level: boxer.level
                };
                this.props.setBoxer(boxer_);
            }
        });
    };

    isPossibleTraining = () => {
        
        const { boxer, boxerActions } = this.props;

        if(boxerActions && boxerActions.length < 4) {

            switch(boxerActions[0].type) {
                case "TRAINING":
                    return boxer.money > boxerActions[0].price && boxer.energy > boxerActions[0].energy;
                case "WORK":
                    return boxer.energy > boxerActions[0].energy;
                default:
                    return false;
            }
        }
    }; 


    runProgressBar = (stepTime) => {

        let percent = 0;

        const interval = setInterval(() => {
            percent += 1;
            this.setState({progressValue: percent});

            if(percent >= 100) {
                this.setState({
                    progressValue: 100,
                    isProgress: false
                });
                const title = this.props.boxerActions[0].name = " was finished !!!";
                this.growl.show({severity: 'success', summary: title, detail: 'Process finished successful'});
                this.props.removeBoxerAction();
                clearInterval(interval);
            };

        }, this.getStepTime(stepTime) );
    };

    getStepTime = (minutes) => {
        const thousandMiliseconds = 1000;
        const hundredPercent = 100;
        const SecondsInOneMinute = 60;

        const totalSeconds = minutes * SecondsInOneMinute;
        const secondsPerPercent = totalSeconds / hundredPercent;
        const milisecondsPerPercent = secondsPerPercent * thousandMiliseconds;

        return milisecondsPerPercent;
    };

    render() {

        const { date, boxer, progressValue, progressTitle } = this.state;
        const { boxerActions } = this.props;

        return (
            <Card title={labels.infoTab.title} subTitle={date} className="mt30">
                <Growl ref={(el) => this.growl = el} />
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.name}
                    </Col>
                    <Col sm={6}>
                        {boxer.name}
                    </Col>
                </Row>
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.surname}
                    </Col>
                    <Col sm={6}>
                        {boxer.surname}
                    </Col>
                </Row>
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.money}
                    </Col>
                    <Col sm={6}>
                        {boxer.money}
                    </Col>
                </Row>
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.energy}
                    </Col>
                    <Col sm={6}>
                        {boxer.energy}
                    </Col>
                </Row>
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.defend}
                    </Col>
                    <Col sm={6}>
                        {boxer.defend}
                    </Col>
                </Row>
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.attack}
                    </Col>
                    <Col sm={6}>
                        {boxer.attack}
                    </Col>
                </Row> 
                <Row>
                    <Col sm={6}>
                        {labels.infoTab.level}
                    </Col>
                    <Col sm={6}>
                        {boxer.level}
                    </Col>
                </Row> 
                { progressValue !== 100 ?
                    <div>
                        <Row className="mt30">
                            <Col sm={12}>
                                <h5>{progressTitle}</h5>
                            </Col>
                            <Col sm={12}>
                                <ProgressBar showValue={true} value={progressValue} />
                            </Col>
                        </Row> 
                        <Col sm={12} className="mt30">
                            <h5>Action queue:</h5>
                        </Col>
                        {boxerActions.slice(1).map(action =>  <Row key={action.id}>
                                                                    <Col sm={6}>
                                                                        <h5>{action.name}:</h5>
                                                                    </Col>
                                                                    <Col sm={6}>
                                                                        <h5>{action.time} min</h5>
                                                                    </Col>
                                                                </Row>)
                                                    }
                    </div> :
                 null }
            </Card>
        );
    };
};

const mapStateToProps = state => ({
    boxerActions: state.boxerActions,
    boxer: state.boxer
});

const mapDispatchToProps = dispatch => ({
    removeBoxerAction: () => dispatch(removeBoxerAction()),
    setBoxer: boxer => dispatch(setBoxer(boxer))
}); 

export default connect(mapStateToProps, mapDispatchToProps)(InfoTab)