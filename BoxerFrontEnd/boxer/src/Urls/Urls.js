export const boxerUrl = "boxerController/getBoxer/";
export const trophiesUrl = "trophyController/getTrophies/";
export const trainingUrl = "trainingController/getTrainings/";
export const trainingByIdUrl = "trainingController/getTrainingById/";
export const workUrl = "workController/getWorks/";
export const foodsUrl = "restaurantController/getFoods/";
export const restUrl = "restController/getRests/";
export const updateBoxerUrl = "boxerController/updateBoxer/"