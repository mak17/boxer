package com.boxer.app.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "works")
public class Work implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "energy")
    private Integer energy;

    @Column(name = "time")
    private Integer time;

    @Column(name = "salary")
    private Integer salary;

    @Column(name = "requiredLevel")
    private Integer requiredLevel;

    public Work() {
    }

    public Work(String name, Integer energy, Integer time, Integer salary, Integer requiredLevel) {
        this.name = name;
        this.energy = energy;
        this.time = time;
        this.salary = salary;
        this.requiredLevel = requiredLevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Integer requiredLevel) {
        this.requiredLevel = requiredLevel;
    }
}
