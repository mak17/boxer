package com.boxer.app.Controllers;

import com.boxer.app.Models.Rest;
import com.boxer.app.Repositories.RestRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/restController")
public class RestController {

    private RestRepository restRepository;

    public RestController(RestRepository restRepository) {
        this.restRepository = restRepository;
    }


    @GetMapping("/getRests/{level}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Collection<Rest> getRests(@PathVariable(value = "level") Integer level) {

        List<Rest> rests = new ArrayList<>();
        try {
            rests = restRepository.getRests(level);
        } catch(NoSuchElementException exception) {
            new IllegalStateException("Cannot find a rests for level " + level);
        }
        return rests;
    }
}