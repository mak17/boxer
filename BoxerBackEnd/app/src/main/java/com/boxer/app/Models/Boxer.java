package com.boxer.app.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "boxers")
public class Boxer implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "energy")
    private Integer energy;

    @Column(name = "attack")
    private Integer attack;

    @Column(name = "defend")
    private Integer defend;

    @Column(name = "money")
    private Integer money;

    @Column(name = "level")
    private Integer level;

    @Column(name = "league")
    private Integer league;

    @Column(name = "fight")
    private Integer fight;

    @Column(name = "ko")
    private Integer ko;

    @Column(name = "won")
    private Integer won;

    @Column(name = "lost")
    private Integer lost;

    @Column(name = "draws")
    private Integer draws;

    @Column(name = "wonRow")
    private Integer wonRow;

    @Column(name = "lostRow")
    private Integer lostRow;

    public Boxer() {
    }

    public Boxer(Long id, String name, String surname, Integer energy, Integer attack, Integer defend, Integer money, Integer level, Integer league, Integer fight, Integer ko, Integer won, Integer lost, Integer draws, Integer wonRow, Integer lostRow) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.energy = energy;
        this.attack = attack;
        this.defend = defend;
        this.money = money;
        this.level = level;
        this.league = league;
        this.fight = fight;
        this.ko = ko;
        this.won = won;
        this.lost = lost;
        this.draws = draws;
        this.wonRow = wonRow;
        this.lostRow = lostRow;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public Integer getDefend() {
        return defend;
    }

    public void setDefend(Integer defend) {
        this.defend = defend;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getLeague() { return league; }

    public void setLeague(Integer league) { this.league = league; }

    public Integer getFight() {
        return fight;
    }

    public void setFight(Integer fight) {
        this.fight = fight;
    }

    public Integer getKo() {
        return ko;
    }

    public void setKo(Integer ko) {
        this.ko = ko;
    }

    public Integer getWon() {
        return won;
    }

    public void setWon(Integer won) {
        this.won = won;
    }

    public Integer getLost() {
        return lost;
    }

    public void setLost(Integer lost) {
        this.lost = lost;
    }

    public Integer getDraws() {
        return draws;
    }

    public void setDraws(Integer draws) {
        this.draws = draws;
    }

    public Integer getWonRow() {
        return wonRow;
    }

    public void setWonRow(Integer wonRow) {
        this.wonRow = wonRow;
    }

    public Integer getLostRow() {
        return lostRow;
    }

    public void setLostRow(Integer lostRow) {
        this.lostRow = lostRow;
    }
}
