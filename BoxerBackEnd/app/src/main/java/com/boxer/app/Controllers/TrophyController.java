package com.boxer.app.Controllers;

import com.boxer.app.Models.Trophy;
import com.boxer.app.Repositories.TrophyRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/trophyController")
public class TrophyController {

    private TrophyRepository trophyRepository;

    public TrophyController(TrophyRepository trophyRepository) {
        this.trophyRepository = trophyRepository;
    }

    @GetMapping("/getTrophies/{boxerId}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Collection<Trophy> getTrophies(@PathVariable(value = "boxerId") Long boxerId) {

        List<Trophy> trophies = new ArrayList<>();
        try {
            trophies = trophyRepository.getTrophies(boxerId);
        } catch(NoSuchElementException exception) {
            new IllegalStateException("Cannot find a trophies for boxer " + boxerId);
        }

        return trophies;
    }
}
