package com.boxer.app.Controllers;

import com.boxer.app.Models.Restaurant;
import com.boxer.app.Repositories.RestaurantRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/restaurantController")
public class RestaurantController {

    private RestaurantRepository restaurantRepository;

    public RestaurantController(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    @GetMapping("/getFoods/{level}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public List<Restaurant> getRestaurants(@PathVariable(value = "level") Integer level) {

        List<Restaurant> restaurants = new ArrayList<>();
        try {
            restaurants = restaurantRepository.getRestaurants(level);
        } catch(NoSuchElementException exception) {
            new IllegalStateException("Cannot find a restaurants for level " + level);
        }
        return restaurants;
    }
}