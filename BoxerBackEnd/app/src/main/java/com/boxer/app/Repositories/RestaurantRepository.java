package com.boxer.app.Repositories;

import com.boxer.app.Models.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {

    @Query(value = "select * from restaurant where required_level <= :level",
            nativeQuery = true)
    List<Restaurant> getRestaurants(@Param("level") Integer level);
}
