package com.boxer.app.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "training")
public class Training implements Serializable {


    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Integer price;

    @Column(name = "attack")
    private Integer attack;

    @Column(name = "defend")
    private Integer defend;

    @Column(name = "energy")
    private Integer energy;

    @Column(name = "requiredLevel")
    private Integer requiredLevel;

    @Column(name = "time")
    private Integer time;

    public Training() {
    }

    public Training(String name, Integer price, Integer attack, Integer defend, Integer energy, Integer requiredLevel, Integer time) {
        this.name = name;
        this.price = price;
        this.attack = attack;
        this.defend = defend;
        this.energy = energy;
        this.requiredLevel = requiredLevel;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public Integer getDefend() {
        return defend;
    }

    public void setDefend(Integer defend) {
        this.defend = defend;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Integer getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Integer requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}