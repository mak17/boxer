package com.boxer.app.Models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "trophies")
public class Trophy implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "dateAward")
    private Date dateAward;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @Column(name = "boxerId")
    private Long boxerId;

    public  Trophy () {}

    public Trophy(Long id, String name, Date dateAward, String description, String image, Long boxerId) {
        this.id = id;
        this.name = name;
        this.dateAward = dateAward;
        this.description = description;
        this.image = image;
        this.boxerId = boxerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateAward() {
        return dateAward;
    }

    public void setDateAward(Date dateAward) {
        this.dateAward = dateAward;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getBoxerId() {
        return boxerId;
    }

    public void setBoxerId(Long boxerId) {
        this.boxerId = boxerId;
    }
}
