package com.boxer.app.Repositories;

import com.boxer.app.Models.Work;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WorkRepository extends JpaRepository<Work, Long> {

    @Query(value = "select * from works where required_level <= :level",
            nativeQuery = true)
    List<Work> getWorks(@Param("level") Integer level);
}
