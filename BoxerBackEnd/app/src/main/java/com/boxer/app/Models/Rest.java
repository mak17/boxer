package com.boxer.app.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rest")
public class Rest implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "energy")
    private Integer energy;

    @Column(name = "time")
    private Integer time;

    @Column(name = "price")
    private Integer price;

    @Column(name = "requiredLevel")
    private Integer requiredLevel;

    public Rest() {
    }

    public Rest(String name, Integer energy, Integer timeInMinutes, Integer price, Integer requiredLevel) {
        this.name = name;
        this.energy = energy;
        this.time = time;
        this.price = price;
        this.requiredLevel = requiredLevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Integer getTimeInMinutes() {
        return time;
    }

    public void setTimeInMinutes(Integer timeInMinutes) {
        this.time = timeInMinutes;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Integer requiredLevel) {
        this.requiredLevel = requiredLevel;
    }
}