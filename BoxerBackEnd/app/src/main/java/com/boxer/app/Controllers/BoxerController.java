package com.boxer.app.Controllers;

import com.boxer.app.Models.Boxer;
import com.boxer.app.Models.Restaurant;
import com.boxer.app.Models.Training;
import com.boxer.app.Models.Work;
import com.boxer.app.Repositories.BoxerRepository;
import com.boxer.app.Repositories.RestaurantRepository;
import com.boxer.app.Repositories.TrainingRepository;
import com.boxer.app.Repositories.WorkRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/boxerController")
public class BoxerController {

    private BoxerRepository boxerRepository;
    private TrainingRepository trainingRepository;
    private WorkRepository workRepository;
    private RestaurantRepository restaurantRepository;

    public BoxerController(BoxerRepository boxerRepository, TrainingRepository trainingRepository, WorkRepository workRepository, RestaurantRepository restaurantRepository) {
        this.boxerRepository = boxerRepository;
        this.trainingRepository = trainingRepository;
        this.workRepository = workRepository;
        this.restaurantRepository = restaurantRepository;
    }

    @GetMapping("/getBoxer/{id}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Boxer getBoxer(@PathVariable(value = "id") Long boxerId) {

        Boxer boxer = boxerRepository.findById(boxerId)
                .orElseThrow(() -> new IllegalStateException("Cannot find a boxer for id " + boxerId));

        return boxer;
    }

    @GetMapping("/updateBoxer/{boxerId}/{actionId}/{actionType}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Boxer doTraining(@PathVariable(value = "boxerId") Long boxerId,
                            @PathVariable(value = "actionId") Long actionId,
                            @PathVariable(value = "actionType") String actionType) {

        Boxer boxer = boxerRepository.findById(boxerId)
                .orElseThrow(() -> new IllegalStateException("Cannot find a boxer for id " + boxerId));

        switch (actionType) {
            case "TRAINING":
                Training training = trainingRepository.findById(actionId)
                        .orElseThrow(() -> new IllegalStateException("Cannot find a training for id " + actionId));

                boxer.setMoney(boxer.getMoney() - training.getPrice());
                boxer.setAttack(boxer.getAttack() + training.getAttack());
                boxer.setDefend(boxer.getDefend() + training.getDefend());
                boxer.setEnergy(boxer.getEnergy() - training.getEnergy());

                return boxerRepository.save(boxer);

            case "WORK":
                Work work = workRepository.findById(actionId)
                        .orElseThrow(() -> new IllegalStateException("Cannot find a work for id " + actionId));

                boxer.setMoney(boxer.getMoney() + work.getSalary());
                boxer.setEnergy(boxer.getEnergy() - work.getEnergy());

                return boxerRepository.save(boxer);
            case "RESTAURANT":
                Restaurant restaurant =restaurantRepository.findById(actionId)
                        .orElseThrow(() -> new IllegalStateException("Cannot find a restaurant for id " + actionId));

                boxer.setMoney(boxer.getMoney() - restaurant.getPrice());
                boxer.setEnergy(boxer.getEnergy() - restaurant.getEnergy());

                return boxerRepository.save(boxer);
            default:
                return boxer;
        }
    }
}