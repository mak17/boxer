package com.boxer.app.Controllers;

import com.boxer.app.Models.Work;
import com.boxer.app.Repositories.WorkRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/workController")
public class WorkController {

    private WorkRepository workRepository;

    public WorkController(WorkRepository workRepository) {
        this.workRepository = workRepository;
    }

    @GetMapping("/getWorks/{level}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Collection<Work> getWorks(@PathVariable(value = "level") Integer level) {

        List<Work> works = new ArrayList<>();
        try {
            works =  workRepository.getWorks(level);
        } catch(NoSuchElementException exception) {
            new IllegalStateException("Cannot find a works for level " + level);
        }

        return works;
    }
}
