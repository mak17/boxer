package com.boxer.app.Repositories;

import com.boxer.app.Models.Trophy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrophyRepository extends JpaRepository<Trophy, Long> {

    @Query(value = "select * from trophies where boxer_id = :boxerId",
            nativeQuery = true)
    List<Trophy> getTrophies(@Param("boxerId") Long boxerId);
}
