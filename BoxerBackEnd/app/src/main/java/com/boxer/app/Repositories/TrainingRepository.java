package com.boxer.app.Repositories;

import com.boxer.app.Models.Training;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrainingRepository extends JpaRepository<Training, Long> {

    @Query(value = "select * from training where required_level <= :level",
            nativeQuery = true)
    List<Training> getTrainings(@Param("level") Integer level);
}