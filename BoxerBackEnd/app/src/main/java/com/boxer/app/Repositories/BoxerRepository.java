package com.boxer.app.Repositories;

import com.boxer.app.Models.Boxer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoxerRepository extends JpaRepository<Boxer, Long> {
}
