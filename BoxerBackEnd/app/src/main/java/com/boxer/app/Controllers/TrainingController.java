package com.boxer.app.Controllers;

import com.boxer.app.Models.Training;
import com.boxer.app.Repositories.TrainingRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/trainingController")
public class TrainingController {

    TrainingRepository trainingRepository;

    public TrainingController(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    @GetMapping("/getTrainings/{level}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Collection<Training> getTrainings(@PathVariable(value = "level") Integer level) {

        List<Training> trainings = new ArrayList<>();
        try {
            trainings = trainingRepository.getTrainings(level);
        } catch(NoSuchElementException exception) {
            new IllegalStateException("Cannot find a trainings for level: " + level);
        }
        return trainings;
    }

    @GetMapping("/getTrainingById/{id}")
    @CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8080"})
    public Training getTrainingById(@PathVariable(value = "id") Long id) {

        Training training = trainingRepository.findById(id)
                .orElseThrow(() -> new IllegalStateException("Cannot find a training for id " + id));

        return training;

    }

}
