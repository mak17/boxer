package com.boxer.app.Repositories;

import com.boxer.app.Models.Rest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface RestRepository extends JpaRepository<Rest, Long> {
    @Query(value = "select * from rest where required_level <= :level",
            nativeQuery = true)
    List<Rest> getRests(@Param("level") Integer level);

}


