CREATE TABLE public.works
(
    id integer NOT NULL,
    name text NOT NULL,
    energy integer NOT NULL,
    timeInMinutes integer NOT NULL,
    salary integer NOT NULL,
    requiredLevel integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.works
    OWNER to postgres;