ALTER TABLE public.boxers ADD COLUMN pkt integer;
	
	ALTER TABLE public.boxers ADD COLUMN league integer;
	
	INSERT INTO public."boxers"(id, name, surname, energy, attack, defend, money, level, pkt, league)
	VALUES 
	(2, 'Adam', 'Abacki', 30, 30, 20, 300, 2, 5, 1),
	(3, 'Rafal', 'Babacki', 40, 40, 40, 300, 2, 7, 1),
	(4, 'Marcin', 'Cabacki', 20, 20, 50, 300, 2, 4, 1),
	(5, 'Robert', 'Dabacki', 60, 50, 50, 300, 2, 3, 1),
	(6, 'Tomek', 'Ebacki', 70, 45, 30, 300, 2, 7, 1),
	(7, 'Mateusz', 'Fabacki', 23, 40, 70, 300, 2, 4, 1),
	(8, 'Dominik', 'Gabacki', 36, 50, 70, 300, 2, 8, 1);
	
	
	UPDATE public.boxers
	SET level=2, pkt=7, league=1
	WHERE id=1;