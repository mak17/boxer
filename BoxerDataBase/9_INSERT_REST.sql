INSERT INTO public.rest(
	id, name, price, energy, "requiredLevel")
	VALUES (1, 'Sleep', 30, 20, 1),
	(1, 'Sleep', 30, 20, 1),
	(2, 'Walk', 35, 30, 1),
	(3, 'SPA', 130, 40, 1),
	(4, 'Massage', 320, 10, 1),
	(5, 'Cinema', 300, 11, 2),
	(6, 'Theater', 50, 13, 2),
	(7, 'Mountains', 40, 14, 1);