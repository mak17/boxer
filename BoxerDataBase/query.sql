INSERT INTO public.boxers(
	id, attack, defend, energy, league, level, money, name, surname)
	VALUES (10, 30, 60, 90, 1, 2, 120, 'Marcin', 'Lawicki');
	
	
INSERT INTO public.trophies(
	id, boxer_id, date_award, description, image, name)
	VALUES (10, 10, '2020-02-14', 'Description 1', 'trophy1', 'Trophy 1'),
	(20, 10, '2019-09-17', 'Description 2', 'trophy6', 'Trophy 2'),
	(30, 10, '2018-07-23', 'Description 3', 'trophy5', 'Trophy 3'),
	(40, 10, '2017-10-30', 'Description 4', 'trophy4', 'Trophy 4'),
	(50, 10, '2015-12-07', 'Description 5', 'trophy5', 'Trophy 5'),
	(60, 10, '2010-05-18', 'Description 6', 'trophy6', 'Trophy 6');
	
	
UPDATE public.boxers
	SET draws=10, fight=50, ko=15, lost=10, lost_row=5, won=30, won_row=12
	WHERE id=10;
	
INSERT INTO public.training(
	id, attack, defend, energy, name, price, required_level, "time")
	VALUES (10, 10, 5, 15, 'Training 1', 1000, 1, 2)
	(20, 20, 15, 25, 'Training 2', 2000, 1, 3),
	(30, 30, 25, 35, 'Training 3', 5000, 2, 4),
	(40, 40, 35, 45, 'Training 4', 7000, 2, 5),
	(50, 50, 45, 45, 'Training 5', 8000, 2, 6),
	(60, 60, 55, 55, 'Training 6', 10000, 2, 7),
	(70, 70, 65, 65, 'Training 7', 12000, 1, 10);
	
INSERT INTO public.works(
	id, energy, name, required_level, salary, "time")
	VALUES (10, 20, 'Work 1', 1, 1000, 1),
	(20, 30, 'Work 2', 1, 4000, 2),
	(30, 40, 'Work 3', 1, 5000, 3),
	(40, 50, 'Work 4', 2, 6000, 4),
	(50, 60, 'Work 5', 2, 7000, 5),
	(60, 70, 'Work 6', 2, 8000, 6),
	(70, 80, 'Work 7', 3, 9000, 7);

INSERT INTO public.restaurant(
	id, energy, name, price, required_level)
	VALUES (10, 20, 'Food 1', 100, 1),
	(20, 30, 'Food 2', 200, 2),
	(30, 40, 'Food 3', 300, 2),
	(40, 50, 'Food 4', 400, 1),
	(50, 60, 'Food 5', 500, 2),
	(60, 70, 'Food 6', 600, 2),
	(70, 80, 'Food 7', 700, 3);

INSERT INTO public.rest(
	id, energy, name, price, required_level, "time")
	VALUES (10, 50, 'Rest 1', 1000, 1, 1),
	(20, 60, 'Rest 2', 2000, 1, 2),
	(30, 70, 'Rest 3', 3000, 1, 3),
	(40, 100, 'Rest 4', 4000, 2, 4),
	(50, 150, 'Rest 5', 5000, 2, 5),
	(60, 120, 'Rest 6', 6000, 2, 6),
	(70, 190, 'Rest 7', 7000, 3, 7);