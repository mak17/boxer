CREATE TABLE public.training
(
    id integer NOT NULL,
    name text NOT NULL,
    price integer NOT NULL,
    attack integer NOT NULL,
    defend integer NOT NULL,
    energy integer NOT NULL,
    requiredlevel integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.training
    OWNER to postgres;