INSERT INTO public.restaurant(
	id, name, price, energy, requiredlevel)
	VALUES (1, 'Coffee', 10, 11, 1),
	(2, 'Apple', 23, 12, 1),
	(3, 'Energy drink', 12, 23, 1),
	(4, 'Tea', 14, 56, 2),
	(5, 'Hamburger', 34, 12, 2),
	(6, 'Banan', 12, 45, 2),
	(7, 'Bread', 2, 21, 1),
	(8, 'Strawberry', 4, 12, 1),
	(9, 'Blackberry', 5, 12, 1),
	(10, 'Pear', 6, 21, 2);