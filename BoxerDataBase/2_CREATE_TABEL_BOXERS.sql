CREATE TABLE public."boxers"
(
    id integer NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    surname text COLLATE pg_catalog."default" NOT NULL,
    energy integer NOT NULL,
    attack integer NOT NULL,
    defend integer NOT NULL,
    money integer NOT NULL,
    CONSTRAINT "Boxer_pkey" PRIMARY KEY (id)
)