INSERT INTO public.training(
	id, name, price, attack, defend, energy, requiredlevel)
	VALUES (1, 'Plane 1', 30, 5, 4, 20, 1),
	 		(2, 'Plane 2', 35, 10, 5, 20, 1),
	  		(3, 'Plane 3', 40, 15, 8, 20, 1),
	   		(4, 'Plane 4', 55, 20, 10, 40, 1),
	    	(5, 'Plane 5', 660, 25, 12, 30, 1),
		 	(6, 'Plane 6', 670, 30, 40, 45, 2),
		  	(7, 'Plane 7', 50, 35, 34, 23, 2),
		   	(8, 'Plane 8', 60, 40, 23, 67, 2),
		    (9, 'Plane 9', 65, 45, 12, 34, 2);