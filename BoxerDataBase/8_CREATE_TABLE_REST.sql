CREATE TABLE public.rest
(
    id integer NOT NULL,
    name text NOT NULL,
    price integer NOT NULL,
    energy integer NOT NULL,
    requiredLevel integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE public.rest
    OWNER to postgres;