INSERT INTO public.works(
	id, name, energy, timeInMinutes, salary, requiredLevel)
	VALUES (1, 'Taxi driver', -8, 1, 50, 1),
	(2, 'Waiter', -4, 2, 10, 1),
	(3, 'Shop assistant', -18, 1, 20, 1),
	(4, 'Security guard', -28, 2, 70, 1),
	(5, 'Supernumerary', -34, 1, 90, 1),
	(6, 'Builder', -44, 1, 100, 2),
	(7, 'Cook', -24, 1, 200, 2),
	(8, 'Fireman', -40, 1, 150, 2);