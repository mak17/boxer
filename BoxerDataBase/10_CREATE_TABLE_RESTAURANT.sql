CREATE TABLE public.restaurant
(
    id integer NOT NULL,
    name text NOT NULL,
    price integer NOT NULL,
    energy integer NOT NULL,
    requiredlevel integer NOT NULL
);

ALTER TABLE public.restaurant
    OWNER to postgres;